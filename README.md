# Widget Informativo para RadioÑú

- - -

## ¿Qué es lo que hace?

  - Permite reproducir la radio directamente desde un navegador que soporte la
etiqueta HTML5 [<audio>] [1] y tenga soporte para [Ogg] [2] [Vorbis] [3].
  - Se comunica en tiempo real con el servidor para obtener información acerca
de la cantidad de ñuescuchas y los datos de la música que está sonando en el
momento, además de indicar si lo que suena es una transmisión en vivo o no.
  - Ofrece un soporte primitivo para navegadores que tengan desactivado JavaScript.

## ¿Cómo funciona?

  - El script debe insertarse en el lugar exacto donde se quiere que aparezca,
debido a que se inserta el HTML necesario para funcionar
  - El script verifica si ya esta cargado jQuery, de lo contrario, agrega una
copia fresca. El script trabaja en [modo de no conflicto] [4], para evitar
problemas.
  - Es posible servir el widget en tu propio espacio de hospedaje, debes copiar
el contenido a la carpeta que deseas usar.

## ¿Cómo lo puedo probar?

Lo puedes ver en acción en la [página del widget] [5].

## ¿Qué planes hay a futuro?

Se tiene considerado convertir el widget en un plugin para WordPress, y se
espera que éste trabajo incentive sus respectivos ports a otros CMS.

[1]: https://www.w3.org/TR/html-markup/audio.html
[2]: https://es.wikipedia.org/wiki/Ogg
[3]: https://es.wikipedia.org/wiki/Vorbis
[4]: https://www.anerbarrena.com/jquery-noconflict-2630/
[5]: https://breadmaker.gitlab.io/radiognu-widget-legacy/
