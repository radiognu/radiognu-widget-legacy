/*
 *  widget-jquery.js -- Widget de RadioÑú
 *
 *  Versión 2.1.0
 *
 *  Copyleft 2011 - 2016 Felipe Peñailillo <breadmaker@radiognu.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301, USA.
 */

// Detectamos o activamos la funcionalidad endsWith, necesaria para loadFile()
if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}

// loadFile(): Carga asíncronamente en el <head> los elementos solicitados
function loadFile(src, callback) {
    var file;
    if (src.endsWith(".js")) {
        file = document.createElement("script");
        file.type = "text/javascript";
        file.src = src;
    } else if (src.endsWith(".css")) {
        file = document.createElement("link");
        file.rel = "stylesheet";
        file.type = "text/css";
        file.href = src;
    }
    document.getElementsByTagName("head")[0].appendChild(file);
    if (typeof callback == "function") {
        var head = document.getElementsByTagName("head")[0] || document.documentElement;
        var done = false;
        file.onload = file.onreadystatechange = function () {
            if (!done && (!this.readyState || this.readyState === "loaded" ||
                    this.readyState === "complete")) {
                done = true;
                callback();
                // Handle memory leak in IE
                file.onload = file.onreadystatechange = null;
                if (head && file.parentNode) {
                    head.removeChild(file);
                }
            }
        };
    }
}

// Detectamos si esta presente jQuery, de lo contrario, cargamos la libreria
if (!window.jQuery) {
    loadFile("https://code.jquery.com/jquery.min.js");
}

// Detectamos si esta presente Socket.IO, de lo contrario, cargamos la libreria.
// Inicializamos el socket luego de la carga
if (!window.io) {
    loadFile(
        "https://cdnjs.cloudflare.com/ajax/libs/socket.io/0.9.17/socket.io.min.js",
        initSocket);
}

// Cargamos el css del widget
loadFile("css/widget.css");

// Previene conflictos de jQuery al definir una variable de llamada propia.
var jQ = jQuery.noConflict();

// Global, usado por clock()
var dialOn = false;

// Global, usado por showTime()
var utcTime = true;

// Global, almacena el elemento <audio> activo, para poder manipularlo
// Ésto permitirá en futuras versiones reemplazar el reproductor propio
// del navegador por el reproductor universal (cuyo avance se puede
// comprobar en https://bitbucket.org/BreadMaker/universal-html5-audio/)
var radio;

// clock(): Muestra la hora, haciendo parpadear el separador de minutos
function clock() {
    showTime();
    if (dialOn) {
        jQ("#wid-time-clock-dial").css("opacity", "0");
    } else {
        jQ("#wid-time-clock-dial").css("opacity", "1");
    }
    dialOn = !dialOn;
}

// showTime(): Obtiene y despliega las horas local y UTC del sistema,
// mostrando la zona horaria correspondiente
function showTime() {
    var d = new Date();
    var h = "0";
    var m = "0";
    if (utcTime) {
        if (d.getUTCHours() < 10) h += d.getUTCHours();
        else h = d.getUTCHours();
        if (d.getUTCMinutes() < 10) m += d.getUTCMinutes();
        else m = d.getUTCMinutes();
        jQ("#wid-time-clock-hour").text(h);
        jQ("#wid-time-clock-minute").text(m);
        jQ("#wid-time-zone").html(jQ("<a/>").attr({
            href: "https://es.wikipedia.org/wiki/Tiempo_universal_coordinado",
            class: "wid-link",
            target: "_blank"
        }).text("UTC"));
    } else {
        if (d.getHours() < 10) h += d.getHours();
        else h = d.getHours();
        if (d.getMinutes() < 10) m += d.getMinutes();
        else m = d.getMinutes();
        jQ("#wid-time-clock-hour").text(h);
        jQ("#wid-time-clock-minute").text(m);
        var tz = String(String(d).split("(")[1]).split(")")[0];
        if (tz == "undefined") jQ("#wid-time-zone").text("Hora local");
        else jQ("#wid-time-zone").html(tz);
    }
}

// showMessage(): Muestra la burbuja de mensaje #wid-message
function showMessage() {
    jQ("#wid-message").animate({
        "margin-top": "0px",
        "opacity": "0.95"
    }, {
        queue: false,
        duration: 750
    });
}

// hideMessage(): Esconde la burbuja de mensaje #wid-message
function hideMessage() {
    jQ("#wid-message").animate({
        "margin-top": "-65px",
        "opacity": "0.0"
    }, {
        queue: false,
        duration: 500
    });
}

var manualFetchTimer = undefined;

// initSocket(): Inicializa la comunicacion en tiempo real con el socket
function initSocket() {
    var socket = io.connect("https://savonet-flows-socket.herokuapp.com", {
        transports: ["websocket", "htmlfile", "xhr-polling",
            "jsonp-polling"
        ]
    });
    socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
    socket.on('joined', function (data) {
        getInfo({
            "artist": data.artist,
            "title": data.title
        });
    });
    socket.on('error', function (data) {
        console.log("Ha ocurrido un error con la conexión.");
        if (manualFetchTimer === undefined) {
            manualFetchTimer = setInterval(function () {
                socket = io.connect(
                    "https://savonet-flows-socket.herokuapp.com", {
                        transports: ["websocket", "htmlfile",
                            "xhr-polling", "jsonp-polling"
                        ]
                    });
                getInfo();
            }, 30000);
        }
    });
    socket.on('disconnect', function () {
        console.log("Se ha perdido la conexión con el socket.");
    });
    socket.on('reconnecting', function () {
        console.log("Se ha iniciado la reconexión con el socket.");
    });
    socket.on('reconnect', function () {
        console.log("La reconexión con el socket ha sido exitosa.");
        socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
    });
    socket.on('reconnect_failed', function () {
        console.log("La reconexión con el socket ha fallado.");
    });
    socket.on("e710da7b9e83debd5dcb4a5455e9998caba8fca7", function (data) {
        if (data.cmd === "metadata") {
            getInfo({
                "artist": data.radio.artist,
                "title": data.radio.title
            });
            console.log(
                "Se han recibidos nuevos metadatos desde el servidor.");
        } else if (data.cmd === "ping radio") {
            console.log(
                "Se he recibido una solicitud de ping desde el servidor.");
        }
    });
}

var artist, title;

// getInfo(): Se comunica con el socket para obtener la información de lo que
// suena por la radio
function getInfo(metadata) {
    if ((navigator.hasOwnProperty("onLine") || Navigator.prototype.hasOwnProperty(
            "onLine")) && navigator.onLine) {
        if (metadata !== undefined) {
            if (jQ(".wid-artista").text() != metadata.artist || jQ(
                    ".wid-titulo")
                .text() != metadata.title) {
                jQ.getJSON("https://radiognu.org/api/?no_cover", metadata,
                    function (data) {
                        processDataFromAPI(data);
                    });
            }
        } else {
            jQ.getJSON("https://radiognu.org/api/?no_cover", function (data) {
                if (artist != data.artist || title != data.title)
                    processDataFromAPI(data);
            });
        }
    }
}

// processDataFromAPI(): Muestra en el widget la información obtenida a traves
// de la API
function processDataFromAPI(data) {
    jQ("#wid-escuchas").text(data.listeners);
    jQ("#wid-live-status").text(data.isLive ? "EN VIVO" : "EN DIFERIDO");
    jQ("#wid-artista").html(data.artist);
    jQ("#wid-titulo").html(data.title);
}

// initWidget(): Inicializa el widget luego de la carga del template
function initWidget() {
    setInterval("clock()", 1000);
    // Obteniendo el alto del mensaje de información para cada botón.
    // Se toma el primero como referencia, se le dan 5px de margen
    var info_h = jQ("#normal-info").height() + 5;
    // Escondiendo elementos
    jQ("#info").hide();
    jQ("#wid-back-button").hide();
    jQ(".wid-info-text").animate({
        opacity: 0.0,
        height: 0
    }, 0);
    // Cerrar el mensaje de tip al hacer click en la "x"
    jQ("#wid-tip-msg-close").click(function () {
        jQ("#wid-tip-msg").hide("slow");
    });
    jQ("#wid-utc-local-time").click(function () {
        utcTime = !utcTime;
        if (!utcTime) jQ(this).attr("title", "Clic para cambiar a hora UTC");
        else jQ(this).attr("title", "Clic para cambiar a hora local");
        showTime();
    });
    // Corrige el bug donde no funcionaban los botones en Konqueror. ¡Gracias, daniel!
    // Esto se solucionará definitivamente en versiones futuras, al utilizar
    // como se debe las clases CSS...
    jQ(".wid-btn-container").each(function (i) {
        switch (i) {
            case 0:
                normlnk = jQ(this).html();
                break;
            case 1:
                lowlnk = jQ(this).html();
                break;
            case 2:
                amlnk = jQ(this).html();
        }
    });
    // Funcionalidades relacionadas con la botonera de selección de señal:
    jQ(".wid-btn-container").click(function () {
        var mount = "";
        if (jQ(this).html() == normlnk || jQ(this).html() == lowlnk ||
            jQ(this).html() == amlnk) {
            jQ(this).next().animate({
                opacity: 0,
                height: 0
            }, 0);
            if (jQ("a", this).text() == "Normal") {
                mount = "radiognu";
                name = "NORMAL";
            } else if (jQ("a", this).text() == "Liviana") {
                mount = "radiognu2";
                name = "LIVIANA";
            } else if (jQ("a", this).text() == "RadioÑú AM") {
                mount = "radiognuam";
                name = "AM";
            }
            jQ("#wid-select-text").text("RadioÑú " + name);
            jQ("#wid-stream-info").html("https://audio.radiognu.org/" + mount +
                ".ogg");
            jQ(this).html(jQ("<audio/>").attr({
                id: "radiognu-audio-" + name.toLowerCase(),
                src: "https://audio.radiognu.org/" + mount + ".ogg",
                controls: "controls",
                autoplay: "autoplay"
            }).html(
                "Tu navegador no soporta el tag HTML5 &lt;audio&gt;"));
            jQ(this).siblings().hide();
            jQ("#wid-stream-info,#wid-back-button").show();
            radio = document.getElementById("radiognu-audio-" + name.toLowerCase());
        }
    });
    // Funciones cuando se presiona el botón "Volver"
    jQ("#wid-back-button").click(function () {
        radio.pause();
        radio.src = "";
        jQ(this).hide();
        jQ("#wid-stream-info").hide();
        jQ(".wid-btn-container").each(function (i) {
            switch (i) {
                case 0:
                    jQ(this).html(normlnk);
                    break;
                case 1:
                    jQ(this).html(lowlnk);
                    break;
                case 2:
                    jQ(this).html(amlnk);
            }
        });
        jQ("#wid-select-text").text("Seleccione una señal");
        jQ(".wid-info-text").css("display", "block");
        jQ(".wid-btn-container,.wid-btn").show();
        // Comprueba por el soporte a la característica history.replaceState
        if (window.history && window.history.replaceState)
            history.replaceState(null, "", document.location.pathname);
        // Si no es soportado, se muestra #! al final de la URL
        else document.location.hash = "!";
    });
    // Efecto cuando se pasa el mouse por sobre un botón de selección de señal
    jQ(".wid-btn,.wid-btn-container").hover(function () {
        jQ(this).next().animate({
            opacity: 1,
            height: info_h
        }, {
            queue: false,
            duration: 100
        });
    }, function () {
        jQ(this).next().animate({
            opacity: 0,
            height: 0
        }, {
            queue: false,
            duration: 300
        });
    });
    // Cierra la burbuja #wid-message cuando se hace click en la X
    jQ("#wid-message-close").click(function () {
        hideMessage();
    });
}

// Funciones para cuando el DOM esté cargado
jQ(document).ready(function () {
    // Buscamos la ubicación de nuestro script en el DOM de la página que lo ha solicitado
    jQ("script").each(function () {
        var that = jQ(this);
        if (that.attr("src") != undefined && that.attr("src").endsWith(
                "radiognu-widget.js")) {
            // Una vez encontrado, obtenemos e insertamos el HTML necesario para
            // que funcione el widget
            jQ.get("js/widget-template.html", function (template) {
                that.after(template);
                // Muestra el widget luego de cargar el template
                jQ("#radiognu-widget").css("height", "auto");
                initWidget();
                // Verifica por el hash si es necesario reproducir alguna señal
                if (location.hash == "#radiognu-normal") {
                    setTimeout("showMessage()", 1000);
                    jQ("#wid-norm-btn").trigger("click");
                    radio = document.getElementById(
                        "radiognu-audio-normal");
                    setTimeout("hideMessage()", 7000);
                }
                if (location.hash == "#radiognu-liviana") {
                    setTimeout("showMessage()", 1000);
                    jQ("#wid-low-btn").trigger("click");
                    radio = document.getElementById(
                        "radiognu-audio-liviana");
                    setTimeout("hideMessage()", 7000);
                }
                if (location.hash == "#radiognu-am") {
                    setTimeout("showMessage()", 1000);
                    jQ("#wid-am-btn").trigger("click");
                    radio = document.getElementById(
                        "radiognu-audio-am");
                    setTimeout("hideMessage()", 7000);
                }
                showTime();
            });
        }
    });
});
